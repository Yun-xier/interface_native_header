/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Web
 * @{
 *
 * @brief 提供注入对象和执行JavaScript代码的API接口。
 * @since 11
 */
/**
 * @file native_interface_arkweb.h
 *
 * @brief 声明API接口供开发者使用注入对象和执行JavaScript代码等功能。
 * @library libohweb.so
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
#ifndef NATIVE_INTERFACE_ARKWEB_H
#define NATIVE_INTERFACE_ARKWEB_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
* @brief 定义执行JavaScript代码后返回结果的回调函数的类型。
*
* @since 11
*/
typedef void (*NativeArkWeb_OnJavaScriptCallback)(const char*);

/**
* @brief 定义注入对象的回调函数的类型。
*
* @since 11
*/
typedef char* (*NativeArkWeb_OnJavaScriptProxyCallback)(const char** argv, int32_t argc);

/**
* @brief 定义Web组件可用时的回调函数的类型。
*
* @since 11
*/
typedef void (*NativeArkWeb_OnValidCallback)(const char*);

/**
* @brief 定义Web组件销毁时的回调函数的类型。
*
* @since 11
*/
typedef void (*NativeArkWeb_OnDestroyCallback)(const char*);

/**
 * @brief 在当前显示页面的环境下，加载并执行一段JavaScript代码。
 *
 * @param webTag Web组件的名称。
 * @param jsCode 一段JavaScript的代码脚本。
 * @param callback 代码执行完后通知开发者结果的回调函数。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
void OH_NativeArkWeb_RunJavaScript(const char* webTag, const char* jsCode, NativeArkWeb_OnJavaScriptCallback callback);

/**
 * @brief 注册对象及函数名称列表。
 *
 * @param webTag Web组件的名称。
 * @param objName 注入对象的名称。
 * @param methodList 注入函数列表的名称。
 * @param callback 注入的回调函数。
 * @param size 注入的回调函数的个数。
 * @param needRefresh 是否需要刷新页面。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
void OH_NativeArkWeb_RegisterJavaScriptProxy(const char* webTag, const char* objName, const char** methodList,
    NativeArkWeb_OnJavaScriptProxyCallback* callback, int32_t size, bool needRefresh);

/**
 * @brief 删除已注册的对象及其下的回调函数。
 *
 * @param webTag Web组件的名称。
 * @param objName 注入对象的名称。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
void OH_NativeArkWeb_UnregisterJavaScriptProxy(const char* webTag, const char* objName);

/**
 * @brief 设置对象可注册时的回调函数。
 *
 * @param webTag Web组件的名称。
 * @param callback 对象可注册时的回调函数。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
void OH_NativeArkWeb_SetJavaScriptProxyValidCallback(const char* webTag, NativeArkWeb_OnValidCallback callback);

/**
 * @brief 获取已注册的对象可注册时的回调函数。
 *
 * @param webTag Web组件的名称。
 * @return return 已注册的对象可注册时的回调函数。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
NativeArkWeb_OnValidCallback OH_NativeArkWeb_GetJavaScriptProxyValidCallback(const char* webTag);

/**
 * @brief 设置组件销毁时的回调函数。
 *
 * @param webTag Web组件的名称。
 * @param callback 组件销毁时的回调函数。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
void OH_NativeArkWeb_SetDestroyCallback(const char* webTag, NativeArkWeb_OnDestroyCallback callback);

/**
 * @brief 获取已注册的组件销毁时的回调函数。
 *
 * @param webTag Web组件的名称。
 * @return return 已注册的组件销毁时的回调函数。
 *
 * @syscap SystemCapability.Web.Webview.Core
 * @since 11
 */
NativeArkWeb_OnDestroyCallback OH_NativeArkWeb_GetDestroyCallback(const char* webTag);

#ifdef __cplusplus
};
#endif
#endif // NATIVE_INTERFACE_ARKWEB_H