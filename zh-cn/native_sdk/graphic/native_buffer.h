/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef NDK_INCLUDE_NATIVE_BUFFER_H_
#define NDK_INCLUDE_NATIVE_BUFFER_H_

/**
 * @addtogroup OH_NativeBuffer
 * @{
 *
 * @brief 提供NativeBuffer功能，通过提供的接口，可以实现共享内存的申请、使用、属性查询、释放等操作。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 9
 * @version 1.0
 */

/**
 * @file native_buffer.h
 *
 * @brief 定义获取和使用NativeBuffer的相关函数。
 *
 * 引用文件<native_buffer/native_buffer.h>
 * @library libnative_buffer.so
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 9
 * @version 1.0
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 提供OH_NativeBuffer结构体声明
 * @since 9
 */
struct OH_NativeBuffer;

/**
 * @brief 提供OH_NativeBuffer结构体声明
 * @since 9
 */
typedef struct OH_NativeBuffer OH_NativeBuffer;

/**
 * @brief OH_NativeBuffer的用途。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 10
 * @version 1.0
 */
enum OH_NativeBuffer_Usage {
    /**
     * CPU可读
     */
    NATIVEBUFFER_USAGE_CPU_READ = (1ULL << 0),
    /**
     * CPU可写
     */
    NATIVEBUFFER_USAGE_CPU_WRITE = (1ULL << 1),
    /**
     * 直接内存访问缓冲区
     */
    NATIVEBUFFER_USAGE_MEM_DMA = (1ULL << 3),
    /**
     * GPU可写
     * @since 12
     */
    NATIVEBUFFER_USAGE_HW_RENDER = (1ULL << 8),
    /**
     * GPU可读
     * @since 12
     */
    NATIVEBUFFER_USAGE_HW_TEXTURE = (1ULL << 9),
    /**
     * CPU可直接映射
     * @since 12
     */
    NATIVEBUFFER_USAGE_CPU_READ_OFTEN = (1ULL << 16),
    /**
     * 512字节对齐
     * @since 12
     */
    NATIVEBUFFER_USAGE_ALIGNMENT_512 = (1ULL << 18),
};

/**
 * @brief OH_NativeBuffer的格式。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 10
 * @version 1.0
 */
enum OH_NativeBuffer_Format {
    /**
     * CLUT8格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_CLUT8 = 0,
    /**
     * CLUT1格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_CLUT1,
    /**
     * CLUT4格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_CLUT4,
    /**
     * RGB565格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_565 = 3,
    /**
     * RGBA5658格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_5658,
    /**
     * RGBX4444格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGBX_4444,
    /**
     * RGBA4444格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_4444,
    /**
     * RGB444格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_444,
    /**
     * RGBX5551格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGBX_5551,
    /**
     * RGBA5551格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_5551,
    /**
     * RGB555格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_555,
    /**
     * RGBX8888格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGBX_8888,
    /**
     * RGBA8888格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_8888,
    /**
     * RGB888格式
     */
    NATIVEBUFFER_PIXEL_FMT_RGB_888,
    /**
     * BGR565格式
     */
    NATIVEBUFFER_PIXEL_FMT_BGR_565,
    /**
     * BGRX4444格式
     */
    NATIVEBUFFER_PIXEL_FMT_BGRX_4444,
    /**
     * BGRA4444格式
     */
    NATIVEBUFFER_PIXEL_FMT_BGRA_4444,
    /**
     * BGRX5551格式
     */
    NATIVEBUFFER_PIXEL_FMT_BGRX_5551,
    /**
     * BGRA5551格式
     */
    NATIVEBUFFER_PIXEL_FMT_BGRA_5551,
    /**
     * BGRX8888格式
     */
    NATIVEBUFFER_PIXEL_FMT_BGRX_8888,
    /**
     * BGRA8888格式
     */
    NATIVEBUFFER_PIXEL_FMT_BGRA_8888,
    /**
     * YUV422 interleaved 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YUV_422_I,
    /**
     * YCBCR422 semi-plannar 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCBCR_422_SP,
    /**
     * YCRCB422 semi-plannar 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCRCB_422_SP,
    /**
     * YCBCR420 semi-plannar 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCBCR_420_SP,
    /**
     * YCRCB420 semi-plannar 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCRCB_420_SP,
    /**
     * YCBCR422 plannar 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCBCR_422_P,
    /**
     * YCRCB422 plannar 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCRCB_422_P,
    /**
     * YCBCR420 plannar 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCBCR_420_P,
    /**
     * YCRCB420 plannar 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCRCB_420_P,
    /**
     * YUYV422 packed 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YUYV_422_PKG,
    /**
     * UYVY422 packed 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_UYVY_422_PKG,
    /**
     * YVYU422 packed 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YVYU_422_PKG,
    /**
     * VYUY422 packed 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_VYUY_422_PKG,
    /**
     * RGBA_1010102 packed 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_RGBA_1010102,
    /**
     * YCBCR420 semi-planar 10bit packed 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCBCR_P010,
    /**
     * YCRCB420 semi-planar 10bit packed 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_YCRCB_P010,
    /**
     * Raw 10bit packed 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_RAW10,
    /**
     * vender mask 格式
     * @since 12
     */
    NATIVEBUFFER_PIXEL_FMT_VENDER_MASK = 0X7FFF0000,
    /**
     * 无效格式
     */
    NATIVEBUFFER_PIXEL_FMT_BUTT = 0X7FFFFFFF
};

/**
 * @brief OH_NativeBuffer的颜色空间
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 11
 * @version 1.0
 */
enum OH_NativeBuffer_ColorSpace {
    /** 无颜色空间 */
    OH_COLORSPACE_NONE,
    /** 色域范围为BT601_P， 传递函数为BT709， 转换矩阵为BT601_P， 数据范围为RANGE_FULL */
    OH_COLORSPACE_BT601_EBU_FULL,
    /** 色域范围为BT601_N， 传递函数为BT709， 转换矩阵为BT601_N， 数据范围为RANGE_FULL */
    OH_COLORSPACE_BT601_SMPTE_C_FULL,
    /** 色域范围为BT709， 传递函数为BT709， 转换矩阵为BT709， 数据范围为RANGE_FULL */
    OH_COLORSPACE_BT709_FULL,
    /** 色域范围为BT2020， 传递函数为HLG， 转换矩阵为BT2020， 数据范围为RANGE_FULL */
    OH_COLORSPACE_BT2020_HLG_FULL,
    /** 色域范围为BT2020， 传递函数为PQ， 转换矩阵为BT2020， 数据范围为RANGE_FULL */
    OH_COLORSPACE_BT2020_PQ_FULL,
    /** 色域范围为BT601_P， 传递函数为BT709， 转换矩阵为BT601_P， 数据范围为RANGE_LIMITED */
    OH_COLORSPACE_BT601_EBU_LIMIT,
    /** 色域范围为BT601_N， 传递函数为BT709， 转换矩阵为BT601_N， 数据范围为RANGE_LIMITED */
    OH_COLORSPACE_BT601_SMPTE_C_LIMIT,
    /** 色域范围为BT709， 传递函数为BT709， 转换矩阵为BT709， 数据范围为RANGE_LIMITED */
    OH_COLORSPACE_BT709_LIMIT,
    /** 色域范围为BT2020， 传递函数为HLG， 转换矩阵为BT2020， 数据范围为RANGE_LIMITED */
    OH_COLORSPACE_BT2020_HLG_LIMIT,
    /** 色域范围为BT2020， 传递函数为PQ， 转换矩阵为BT2020， 数据范围为RANGE_LIMITED */
    OH_COLORSPACE_BT2020_PQ_LIMIT,
    /** 色域范围为SRGB， 传递函数为SRGB， 转换矩阵为BT601_N， 数据范围为RANGE_FULL */
    OH_COLORSPACE_SRGB_FULL,
    /** 色域范围为P3_D65， 传递函数为SRGB， 转换矩阵为P3， 数据范围为RANGE_FULL */
    OH_COLORSPACE_P3_FULL,
    /** 色域范围为P3_D65， 传递函数为HLG， 转换矩阵为P3， 数据范围为RANGE_FULL */
    OH_COLORSPACE_P3_HLG_FULL,
    /** 色域范围为P3_D65， 传递函数为PQ， 转换矩阵为P3， 数据范围为RANGE_FULL */
    OH_COLORSPACE_P3_PQ_FULL,
    /** 色域范围为ADOBERGB， 传递函数为ADOBERGB， 转换矩阵为ADOBERGB， 数据范围为RANGE_FULL */
    OH_COLORSPACE_ADOBERGB_FULL,
    /** 色域范围为SRGB， 传递函数为SRGB， 转换矩阵为BT601_N， 数据范围为RANGE_LIMITED */
    OH_COLORSPACE_SRGB_LIMIT,
    /** 色域范围为P3_D65， 传递函数为SRGB， 转换矩阵为P3， 数据范围为RANGE_LIMITED */
    OH_COLORSPACE_P3_LIMIT,
    /** 色域范围为P3_D65， 传递函数为HLG， 转换矩阵为P3， 数据范围为RANGE_LIMITED */
    OH_COLORSPACE_P3_HLG_LIMIT,
    /** 色域范围为P3_D65， 传递函数为PQ， 转换矩阵为P3， 数据范围为RANGE_LIMITED */
    OH_COLORSPACE_P3_PQ_LIMIT,
    /** 色域范围为ADOBERGB， 传递函数为ADOBERGB， 转换矩阵为ADOBERGB， 数据范围为RANGE_LIMITED */
    OH_COLORSPACE_ADOBERGB_LIMIT,
    /** 色域范围为SRGB， 传递函数为LINEAR */
    OH_COLORSPACE_LINEAR_SRGB,
    /** 等同于 OH_COLORSPACE_LINEAR_SRGB */
    OH_COLORSPACE_LINEAR_BT709,
    /** 色域范围为P3_D65， 传递函数为LINEAR */
    OH_COLORSPACE_LINEAR_P3,
    /** 色域范围为BT2020， 传递函数为LINEAR */
    OH_COLORSPACE_LINEAR_BT2020,
    /** 等同于 OH_COLORSPACE_SRGB_FULL */
    OH_COLORSPACE_DISPLAY_SRGB,
    /** 等同于 OH_COLORSPACE_P3_FULL */
    OH_COLORSPACE_DISPLAY_P3_SRGB,
    /** 等同于 OH_COLORSPACE_P3_HLG_FULL */
    OH_COLORSPACE_DISPLAY_P3_HLG,
    /** 等同于 OH_COLORSPACE_P3_PQ_FULL */
    OH_COLORSPACE_DISPLAY_P3_PQ,
    /** 色域范围为BT2020， 传递函数为SRGB， 转换矩阵为BT2020， 数据范围为RANGE_FULL */
    OH_COLORSPACE_DISPLAY_BT2020_SRGB,
    /** 等同于 OH_COLORSPACE_BT2020_HLG_FULL */
    OH_COLORSPACE_DISPLAY_BT2020_HLG,
    /** 等同于 OH_COLORSPACE_BT2020_PQ_FULL */
    OH_COLORSPACE_DISPLAY_BT2020_PQ,
};

/**
 * @brief OH_NativeBuffer的转换类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
enum OH_NativeBuffer_TransformType {
    /** 不旋转 */
    NATIVEBUFFER_ROTATE_NONE = 0,
    /** 旋转90度 */
    NATIVEBUFFER_ROTATE_90,
    /** 旋转180度 */
    NATIVEBUFFER_ROTATE_180,
    /** 旋转270度 */
    NATIVEBUFFER_ROTATE_270,
    /** 水平翻转 */
    NATIVEBUFFER_FLIP_H,
    /** 垂直翻转 */
    NATIVEBUFFER_FLIP_V,
    /** 水平翻转并旋转90度 */
    NATIVEBUFFER_FLIP_H_ROT90,
    /** 垂直翻转并旋转90度 */
    NATIVEBUFFER_FLIP_V_ROT90,
    /** 水平翻转并旋转180度 */
    NATIVEBUFFER_FLIP_H_ROT180,
    /** 垂直翻转并旋转180度 */
    NATIVEBUFFER_FLIP_V_ROT180,
    /** 水平翻转并旋转270度 */
    NATIVEBUFFER_FLIP_H_ROT270,
    /** 垂直翻转并旋转270度 */
    NATIVEBUFFER_FLIP_V_ROT270,
};

/**
 * @brief OH_NativeBuffer的色域。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
enum OH_NativeBuffer_ColorGamut {
    /** 默认色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_NATIVE = 0,
    /** Standard BT601 色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_STANDARD_BT601 = 1,
    /** Standard BT709 色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_STANDARD_BT709 = 2,
    /** DCI P3 色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_DCI_P3 = 3,
    /** SRGB 色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_SRGB = 4,
    /** Adobe RGB 色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_ADOBE_RGB = 5,
    /** Display P3 色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_DISPLAY_P3 = 6,
    /** BT2020 色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_BT2020 = 7,
    /** BT2100 PQ 色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_BT2100_PQ = 8,
    /** BT2100 HLG 色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_BT2100_HLG = 9,
    /** Display BT2020 色域格式 */
    NATIVEBUFFER_COLOR_GAMUT_DISPLAY_BT2020 = 10,
};

/**
 * @brief OH_NativeBuffer的属性配置，用于申请新的OH_NativeBuffer实例或查询现有实例的相关属性
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 9
 * @version 1.0
 */
typedef struct OH_NativeBuffer_Config {
    /**
     * 宽度（像素）
     */
    int32_t width;
    /**
     * 高度（像素）
     */
    int32_t height;
    /**
     * 像素格式
     */
    int32_t format;
    /**
     * buffer的用途说明
     */
    int32_t usage;
    /**
     * 本地窗口缓冲区步幅
     * @since 10
     */
    int32_t stride;
} OH_NativeBuffer_Config;

/**
 * @brief 单个图像平面格式信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_Plane {
    /**
     * 图像平面的偏移量，单位为Byte。
     */
    uint64_t offset;
    /**
     * 从图像一行的第一个值到下一行的第一个值的距离，单位为Byte。
     */
    uint32_t rowStride;
    /**
     * 从图像一列的第一个值到下一列的第一个值的距离，单位为Byte。
     */
    uint32_t columnStride;
} OH_NativeBuffer_Plane;

/**
 * @brief OH_NativeBuffer的图像平面格式信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @since 12
 * @version 1.0
 */
typedef struct OH_NativeBuffer_Planes {
    /**
     * 不同平面的数量。
     */
    uint32_t planeCount;
    /**
     * 图像平面格式信息数组。
     */
    OH_NativeBuffer_Plane planes[4];
} OH_NativeBuffer_Planes;


/**
 * @brief 通过OH_NativeBuffer_Config创建OH_NativeBuffer实例，每次调用都会产生一个新的OH_NativeBuffer实例
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param config 一个指向OH_NativeBuffer_Config类型的指针
 * @return 创建成功则返回一个指向OH_NativeBuffer结构体实例的指针，否则返回NULL
 * @since 9
 * @version 1.0
 */
OH_NativeBuffer* OH_NativeBuffer_Alloc(const OH_NativeBuffer_Config* config);

/**
 * @brief 将OH_NativeBuffer对象的引用计数加1
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer 一个指向OH_NativeBuffer实例的指针
 * @return 返回值为0表示执行成功
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Reference(OH_NativeBuffer *buffer);

/**
 * @brief 将OH_NativeBuffer对象的引用计数减1，当引用计数为0的时候，该NativeBuffer对象会被析构掉
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer 一个指向OH_NativeBuffer实例的指针
 * @return 返回值为0表示执行成功
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Unreference(OH_NativeBuffer *buffer);

/**
 * @brief 用于获取OH_NativeBuffer的属性
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer 一个指向OH_NativeBuffer实例的指针
 * @param config 一个指向OH_NativeBuffer_Config的指针，用于接收OH_NativeBuffer的属性
 * @since 9
 * @version 1.0
 */
void OH_NativeBuffer_GetConfig(OH_NativeBuffer *buffer, OH_NativeBuffer_Config* config);

/**
 * @brief 将OH_NativeBuffer对应的ION内存映射到进程空间
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer 一个指向OH_NativeBuffer实例的指针
 * @param virAddr 一个二级指针，二级指针指向映射到当前进程的虚拟内存的地址
 * @return 返回值为0表示执行成功
 * @since 9
 * @version 1.0
 */

int32_t OH_NativeBuffer_Map(OH_NativeBuffer *buffer, void **virAddr);

/**
 * @brief 将OH_NativeBuffer对应的ION内存从进程空间移除
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer 一个指向OH_NativeBuffer实例的指针
 * @return 返回值为0表示执行成功
 * @since 9
 * @version 1.0
 */
int32_t OH_NativeBuffer_Unmap(OH_NativeBuffer *buffer);

/**
 * @brief 获取OH_NativeBuffer的序列号
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer 一个指向OH_NativeBuffer实例的指针
 * @return 返回对应OH_NativeBuffer的唯一序列号
 * @since 9
 * @version 1.0
 */
uint32_t OH_NativeBuffer_GetSeqNum(OH_NativeBuffer *buffer);

/**
 * @brief 为OH_NativeBuffer设置颜色空间属性
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer 一个指向OH_NativeBuffer实例的指针
 * @param colorSpace 为OH_NativeBuffer设置的颜色空间，其值从OH_NativeBuffer_ColorSpace获取
 * @return 返回值为0表示执行成功
 * @since 11
 * @version 1.0
 */
int32_t OH_NativeBuffer_SetColorSpace(OH_NativeBuffer *buffer, OH_NativeBuffer_ColorSpace colorSpace);

/**
 * @brief 将OH_NativeBuffer对应的多通道ION内存映射到进程空间。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param buffer 一个指向OH_NativeBuffer实例的指针。
 * @param virAddr 一个二级指针，二级指针指向映射到当前进程的虚拟内存的地址。
 * @param outPlanes 一个指向所有图像平面格式信息的指针。
 * @return 返回值为0表示执行成功。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeBuffer_MapPlanes(OH_NativeBuffer *buffer, void **virAddr, OH_NativeBuffer_Planes *outPlanes);

/**
 * @brief 将OHNativeWindowBuffer实例转换为OH_NativeBuffer实例.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeBuffer
 * @param nativeWindowBuffer 一个指向OHNativeWindowBuffer实例的指针。
 * @param buffer 一个指向OH_NativeBuffer实例的指针。
 * @return 返回值为0表示执行成功。
 * @since 12
 * @version 1.0
 */
int32_t OH_NativeBuffer_FromNativeWindowBuffer(OHNativeWindowBuffer *nativeWindowBuffer, OH_NativeBuffer **buffer);

#ifdef __cplusplus
}
#endif

/** @} */
#endif