/*
 * Copyright (C) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup AVPlayer
 * @{
 *
 * @brief 为媒体源提供播放能力的API。
 *
 * @Syscap SystemCapability.Multimedia.Media.AVPlayer
 * @since 11
 * @version 1.0
 */

/**
 * @file avplayer.h
 *
 * @brief定义avplayer接口。使用AVPlayer提供的Native API播放媒体源

 *
 * @library libavplayer.so
 * @since 11
 * @version 1.0
 */

#ifndef MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_H
#define MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_H

#include <stdint.h>
#include <stdio.h>
#include "native_averrors.h"
#include "avplayer_base.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct MediaKeySession MediaKeySession;
typedef struct DRM_MediaKeySystemInfo DRM_MediaKeySystemInfo;
typedef void (*Player_MediaKeySystemInfoCallback)(OH_AVPlayer *play, DRM_MediaKeySystemInfo* mediaKeySystemInfo);

/**
 * @brief 创建播放器
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @return 返回指向OH_AVPlayer实例的指针
 * @since 11
 * @version 1.0
*/
OH_AVPlayer *OH_AVPlayer_Create(void);

/**
 * @brief 设置播放器的播放源。对应的源可以是http url
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param url播放源
 * @return 如果url设置成功返回{@link AV_ERR_OK}，否则返回{@link native_averrors.h}中定义的错误码
 * in {@link native_averrors.h} otherwise.
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetURLSource(OH_AVPlayer *player, const char *url);

/**
 * @brief 设置播放器的播放媒体文件描述符来源
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param fd媒体源的文件描述符
 * @param offset媒体源在文件描述符中的偏移量
 * @param  size表示媒体源的大小
 * @return 如果fd设置成功返回{@link AV_ERR_OK}，否则返回{@link native_averrors.h}中定义的错误码
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetFDSource(OH_AVPlayer *player, int32_t fd, int64_t offset, int64_t size);

/**
 * @brief 准备播放环境，异步缓存媒体数据
 *
 * 此函数必须在{@link SetSource}之后调用
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果成功将{@link准备}添加到任务队列中，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Prepare(OH_AVPlayer *player);

/**
 * @brief 开始播放
 *
 * 此函数必须在{@link Prepare}之后调用。如果播放器状态为<Prepared>。调用此函数开始播放。
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果开始播放，则返回{@link AV_ERR_OK}，否则返回在{@link native_averrors.h}中定义的错误代码
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Play(OH_AVPlayer *player);

/**
 * @brief 暂停播放.
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果成功将{@link Pause}添加到任务队列中，否则返回在{@link native_averrors.h}中定义的错误代码
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Pause(OH_AVPlayer *player);

/**
 * @brief 停止播放.
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果成功将{@link stop}添加到任务队列，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Stop(OH_AVPlayer *player);

/**
 * @brief 将播放器恢复到初始状态
 *
 * 函数调用完成后，调用{@link SetSource}添加播放源。调用{@link Prepare}后，调用{@link Play}重新开始播放。
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果成功将{@link reset}添加到任务队列，否则返回在{@link native_averrors.h}中定义的错误代码
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Reset(OH_AVPlayer *player);

/**
 * @brief 异步释放播放器资源
 *
 * 异步释放保证性能，但无法保证是否释放了播放画面的surfacebuffer。调用者需要保证播放画面窗口的生命周期安全
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果成功将{@link Release}添加到任务队列中，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_Release(OH_AVPlayer *player);

/**
 * @brief 同步释放播放器资源
 *
 * 同步过程保证了播放画面的surfacebuffer释放，但这个界面会花费很长时间（发动机非怠速状态时），要求调用者自己设计异步机制
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果播放被释放返回{@link AV_ERR_OK}，否则返回在{@link native_averrors.h}中定义的错误代码
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_ReleaseSync(OH_AVPlayer *player);

/**
 * @brief 设置播放器的音量
 *
 * 可以在播放或暂停的过程中使用。<0>表示无声音。,<1>为原始值。

 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param 要设置的左声道目标音量
 * @param 要设置的右声道目标音量
 * @return 如果设置了音量，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetVolume(OH_AVPlayer *player, float leftVolume, float rightVolume);

/**
 * @brief 改变播放位置
 *
 * 此函数可以在播放或暂停时使用.
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param mSeconds播放目标位置，精确到毫秒
 * @param mode播放器的跳转模式。具体请参见{@link AVPlayerSeekMode}
 * @return 如果完成跳转，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
*/
OH_AVErrCode OH_AVPlayer_Seek(OH_AVPlayer *player, int32_t mSeconds, AVPlayerSeekMode mode);

/**
 * @brief 获取播放位置，精确到毫秒
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param currentTime播放位置
 * @return 如果获取当前位置，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetCurrentTime(OH_AVPlayer *player, int32_t *currentTime);

/**
 * @brief 获取视频宽度
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param videoWidth视频宽度
 * @return 如果获取视频宽度，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetVideoWidth(OH_AVPlayer *player, int32_t *videoWidth);

/**
 * @brief 获取视频高度
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param videoHeights视频高度
 * @return 如果获取视频高度，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetVideoHeight(OH_AVPlayer *player, int32_t *videoHeight);

/**
 * @brief 设置播放器播放速率
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param speed可以设置速率模式{@link AVPlaybackSpeed}。
 * @return 设置播放速率成功返回{@link AV_ERR_OK}，否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetPlaybackSpeed(OH_AVPlayer *player, AVPlaybackSpeed speed);

/**
 * @brief 获取当前播放器播放速率
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param speed 可以获取的速率模式{@link AVPlaybackSpeed}
 * @return 获取播放速率成功返回{@link AV_ERR_OK}，否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetPlaybackSpeed(OH_AVPlayer *player, AVPlaybackSpeed *speed);

/**
 * @brief 设置hls播放器使用的码率
 *
 * 播放比特率，以比特/秒为单位，以比特/秒为单位。
 * 仅对HLS协议网络流有效。默认情况下，
 * 播放器会根据网络连接情况选择合适的码率和速度。
 * 通过INFO_TYPE_BITRATE_COLLECT上报有效码率链表
 * 设置并选择指定的码率，选择小于和最接近的码率
 * 到指定的码率播放。准备好后，读取它以查询当前选择的比特率。
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param bitRate码率，单位为bps
 * @return 设置码率成功返回{@link AV_ERR_OK}，否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SelectBitRate(OH_AVPlayer *player, uint32_t bitRate);

/**
 * @brief 设置播放画面窗口.
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param window指向OHNativeWindow实例的指针，参见{@link OHNativeWindow}
 * @return 如果设置播放画面窗口成功，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode  OH_AVPlayer_SetVideoSurface(OH_AVPlayer *player, OHNativeWindow *window);

/**
 * @brief 获取媒体文件的总时长，精确到毫秒
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param duration媒体文件的总时长
 * @return 如果获取当前时长，则返回{@link AV_ERR_OK}，否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetDuration(OH_AVPlayer *player, int32_t *duration);

/**
 * @brief 获取当前播放状态
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param state 当前播放状态
 * @return 如果获取当前播放状态，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetState(OH_AVPlayer *player, AVPlayerState *state);

/**
 * @brief 判断播放器是否在播放
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果正在播放，则返回true，否则返回false
 * @since 11
 * @version 1.0
 */
bool OH_AVPlayer_IsPlaying(OH_AVPlayer *player);

/**
 * @brief 判断是用循环播放
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @return 如果循环播放，则返回true，否则返回false。
 * @since 11
 * @version 1.0
 */
bool OH_AVPlayer_IsLooping(OH_AVPlayer *player);

/**
 * @brief 设置循环播放
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param loop 循环播放开关
 * @return 如果设置了循环，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetLooping(OH_AVPlayer *player, bool loop);

/**
 * @brief 设置播放器回调方法
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param callback 回调对象指针
 * @return 如果设置了播放器回调，则返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetPlayerCallback(OH_AVPlayer *player, AVPlayerCallback callback);

/**
 * @brief 选择音频或字幕轨道
 *
 * 默认播放第一个带数据的音频流，不播放字幕轨迹。
 * 设置生效后，原曲目将失效。请设置字幕
 * 处于准备/播放/暂停/完成状态，并将音轨设置为准备状态。
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param index 索引
 * @return 如果成功选择返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
*/
OH_AVErrCode OH_AVPlayer_SelectTrack(OH_AVPlayer *player, int32_t index);

/**
 * @brief 取消选择当前音频或字幕轨道
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param index 索引
 * @return 如果成功返回{@link AV_ERR_OK}；否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 11
 * @version 1.0
*/
OH_AVErrCode OH_AVPlayer_DeselectTrack(OH_AVPlayer *player, int32_t index);

/**
 * @brief 获取当前有效的轨道索引
 *
 * 请将其设置为准备/正在播放/暂停/完成状态
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param trackType 媒体类型
 * @param index 索引
 * @return 
 * @since 11
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetCurrentTrack(OH_AVPlayer *player, int32_t trackType, int32_t *index);

/**
 * @brief 设置播放器媒体密钥系统信息回调的方法
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param callback 对象指针
 * @return 成功返回{@link AV_ERR_OK}，否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 12
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_SetMediaKeySystemInfoCallback(OH_AVPlayer *player,
    Player_MediaKeySystemInfoCallback callback);

/**
 * @brief 获取媒体密钥系统信息以创建媒体密钥会话
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param mediaKeySystemInfo 媒体密钥系统信息
 * @return 成功返回{@link AV_ERR_OK}，否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 12
 * @version 1.0
 */
OH_AVErrCode OH_AVPlayer_GetMediaKeySystemInfo(OH_AVPlayer *player, DRM_MediaKeySystemInfo *mediaKeySystemInfo);

/**
 * @brief 设置解密信息
 *
 * @syscap SystemCapability.Multimedia.Media.AVPlayer
 * @param player指向OH_AVPlayer实例的指针
 * @param mediaKeySession 具有解密功能的媒体密钥会话实例
 * @param secureVideoPath 是否需要安全解码器
 * @return 成功返回{@link AV_ERR_OK}，否则返回在{@link native_averrors.h}中定义的错误代码。
 * @since 12
 * @version 1.0
*/
OH_AVErrCode OH_AVPlayer_SetDecryptionConfig(OH_AVPlayer *player, MediaKeySession *mediaKeySession,
    bool secureVideoPath);

#ifdef __cplusplus
}
#endif

#endif // MULTIMEDIA_PLAYER_FRAMEWORK_NATIVE_AVPLAYER_H
