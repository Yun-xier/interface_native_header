/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup MediaAssetManager
 * @{
 *
 *
 * @brief 提供媒体源请求能力的API。
 *
 * OH_MediaAssetManager结构体和请求Id用于请求媒体库资源。
 * 可以使用请求Id取消请求。
 *
 * @since 12
 */

/**
 * @file media_asset_manager.h
 *
 * @brief 定义了媒体资产管理器的结构和枚举。
 *
 * OH_MediaAssetManager结构体：该结构体提供了从媒体库请求资源的能力。 \n
 * MediaLibrary_RequestId结构体：在请求媒体库资源时返回的类型。请求Id用于取消请求。 \n
 * MediaLibrary_DeliveryMode枚举：此枚举定义了请求资源的分发模式。 \n
 * OH_MediaLibrary_OnDataPrepared函数指针：当所请求的媒体资源准备完成时会触发回调。 \n
 * MediaLibrary_RequestOptions结构体：此结构体为媒体资源请求策略模式配置项。 \n
 *
 * @Syscap SystemCapability.FileManagement.PhotoAccessHelper.Core
 * @library libmedia_asset_manager.so
 * @since 12
 */

#ifndef MULTIMEDIA_MEDIA_LIBRARY_NATIVE_MEDIA_ASSET_BASE_H
#define MULTIMEDIA_MEDIA_LIBRARY_NATIVE_MEDIA_ASSET_BASE_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 定义UUID最大长度。
 *
 * 这个常量定义了UUID字符串的最大长度。
 *
 * @since 12
 */
static const int32_t UUID_STR_MAX_LENGTH = 37;

/**
 * @brief 定义媒体资产管理器。
 *
 * 此结构提供了请求媒体库资源的能力。
 * 如果创建失败，则返回空指针。
 *
 * @since 12
 */
typedef struct OH_MediaAssetManager OH_MediaAssetManager;

/**
 * @brief 定义请求Id。
 *
 * 当请求媒体库资源时，会返回此类型。
 * 请求Id用于取消请求。
 * 如果请求失败，值将全为零，如 "00000000-0000-0000-0000-000000000000"。
 *
 * @since 12
 */
typedef struct MediaLibrary_RequestId {
    /** 请求Id。 */
    char requestId[UUID_STR_MAX_LENGTH];
} MediaLibrary_RequestId;

/**
 * @brief 请求资源分发模式。
 *
 * 此枚举定义了请求资源的分发模式。
 * 快速分发：不考虑资源质量，直接基于现有资源返回。 \n
 * 高质量分发：返回高质量资源，若没有，则触发生成高质量资源，成功后才返回。 \n
 * 均衡分发：若存在高质量资源，则直接返回高质量资源。
 * 否则，先返回低质量资源，并触发生成高质量资源，成功后再返回一次高质量资源。 \n
 *
 * @since 12
 */
typedef enum MediaLibrary_DeliveryMode {
    /** 快速分发。 */
    MEDIA_LIBRARY_FAST_MODE = 0,
    /** 高质量分发。 */
    MEDIA_LIBRARY_HIGH_QUALITY_MODE = 1,
    /** 均衡分发。 */
    MEDIA_LIBRARY_BALANCED_MODE = 2
} MediaLibrary_DeliveryMode;

/**
 * @brief 当所请求的媒体资源准备完成时会触发回调。
 *
 * @param result 请求资源处理的结果。
 * @param requestId 请求Id。
 * @since 12
 */
typedef void (*OH_MediaLibrary_OnDataPrepared)(int32_t result, MediaLibrary_RequestId requestId);

/**
 * @brief 请求策略模式配置项。
 *
 * 此结构体为媒体资源请求策略模式配置项。
 *
 * @since 12
 */
typedef struct MediaLibrary_RequestOptions {
    /** 分发模式。 */
    MediaLibrary_DeliveryMode deliveryMode;
} MediaLibrary_RequestOptions;

#ifdef __cplusplus
}
#endif
#endif // MULTIMEDIA_MEDIA_LIBRARY_NATIVE_MEDIA_ASSET_BASE_H