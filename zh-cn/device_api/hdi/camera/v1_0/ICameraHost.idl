/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file ICameraHost.idl
 *
 * @brief Camera服务的管理类，对上层提供HDI接口。

 *
 * 模块包路径：ohos.hdi.camera.v1_0
 *
 * 引用：
 * - ohos.hdi.camera.v1_0.ICameraHostCallback
 * - ohos.hdi.camera.v1_0.ICameraDeviceCallback
 * - ohos.hdi.camera.v1_0.ICameraDevice
 *
 * @since 3.2
 * @version 1.0
 */


package ohos.hdi.camera.v1_0;

import ohos.hdi.camera.v1_0.ICameraHostCallback;
import ohos.hdi.camera.v1_0.ICameraDeviceCallback;
import ohos.hdi.camera.v1_0.ICameraDevice;

/**
 * @brief 定义Camera设备功能操作。
 *
 * 设置回调接口、返回设备ID列表、打开并执行Camera设备的相关操作。
 *
 * @since 3.2
 * @version 1.0
 */
interface ICameraHost {
     /**
     * @brief 设置ICameraHost回调接口，回调函数参考{@link ICameraHostCallback}。
     *
     * @param callbackObj 要设置的回调函数。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @since 3.2
     * @version 1.0
     */
    SetCallback([in] ICameraHostCallback callbackObj);

     /**
     * @brief 获取当前可用的Camera设备ID列表。
     *
     * @param cameraIds 返回当前可用的设备列表。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @see GetCameraAbility
     *
     * @since 3.2
     * @version 1.0
     */
    GetCameraIds([out] String[] cameraIds);

     /**
     * @brief 获取Camera设备能力集合。
     *
     * @param cameraId 用于指定要操作的Camera设备，通过{@link GetCameraIds}获取。
     * @param cameraAbility 返回cameraId对应Camera设备的能力集合。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @see GetCameraIds
     *
     * @since 3.2
     * @version 1.0
     */
    GetCameraAbility([in] String cameraId, [out] unsigned char[] cameraAbility);

     /**
     * @brief 打开Camera设备。
     *
     * 打开指定的Camera设备，通过此接口可以获取到ICameraDevice对象，通过ICameraDevice对象可以操作具体的Camera设备。
     *
     * @param cameraId 需要打开的Camera设备ID，可通过{@link GetCameraIds}接口获取当前已有Camera设备列表。
     * @param callbackObj Camera设备相关的回调函数，具体参见{@link ICameraDeviceCallback}。
     * @param device 返回当前要打开的Camera设备ID对应的ICameraDevice对象。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @see GetCameraIds
     *
     * @since 3.2
     * @version 1.0
     */
    OpenCamera([in] String cameraId, [in] ICameraDeviceCallback callbackObj, [out] ICameraDevice device);

     /**
     * @brief 打开或关闭闪光灯。
     *
     * 该接口只能由打开cameraId指定Camera设备的调用者调用。
     *
     * @param cameraId 闪光灯对应的Camera设备ID。
     * @param isEnable 表示是否打开闪光灯，true表示打开，false表示关闭。
     *
     * @return NO_ERROR 表示执行成功。
     * @return 其他值表示执行失败，具体错误码查看{@link CamRetCode}。
     *
     * @see GetCameraIds
     *
     * @since 3.2
     * @version 1.0
     */
    SetFlashlight([in] String cameraId, [in] boolean isEnable);
}
/** @} */