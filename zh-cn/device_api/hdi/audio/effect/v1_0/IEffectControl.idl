/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiEffect
 * @{
 *
 * @brief Effect模块接口定义。
 *
 * 音效接口涉及数据类型、音效模型接口、音效控制器接口等。
 *
 * @since 4.0
 * @version 1.0
 */
 
 /**
 * @file IEffectControl.idl
 *
 * @brief 音效控制器的接口定义文件。
 *
 * 模块包路径：ohos.hdi.audio.effect.v1_0
 *
 * 引用：ohos.hdi.audio.effect.v1_0.EffectTypes
 *
 * @since 4.0
 * @version 1.0
 */


package ohos.hdi.audio.effect.v1_0;
import ohos.hdi.audio.effect.v1_0.EffectTypes;

/**
 * @brief 音效控制器接口。
 *
 * 提供音效控制器支持的驱动能力，包括音效数据处理、音效命令发送、获取当前音效描述符等。
 *
 * @since 4.0
 * @version 1.0
 */
interface IEffectControl {
    /**
     * @brief 处理音频原始数据。
     *
     * 必须指定输入和输出buffer，如果未指定，则进程必须使用命令提供的数据处理功能。
     *
     * @param control 指向要调用该接口的音效控件，该指针参数在编译为C接口后产生。
     * @param input 输入数据的buffer
     * @param output 输出数据的buffer
     *
     * @return 执行成功返回0，执行失败返回其他值。
     *
     * @since 4.0
     * @version 1.0
     */
    EffectProcess([in] struct AudioEffectBuffer input, [out] struct AudioEffectBuffer output);

    /**
     * @brief 发送音效处理命令。
     *
     * @param control 指向要调用该接口的音效控件，该指针参数在编译为C接口后产生。
     * @param cmdId 用于匹配命令表中的命令选项的命令索引
     * @param cmdData 系统服务的数据。
     * @param cmdDataLen 数据长度，该参数在编译为C接口后产生。
     * @param replyData 返回的数据。
     * @param replyDataLen 数据长度，该参数在编译为C接口后产生。
     *
     * @return 执行成功返回0，执行失败返回其他值。
     *
     * @since 4.0
     * @version 1.0
     */
    SendCommand([in] unsigned int cmdId, [in] byte[] cmdData, [out] byte[] replyData);

    /**
     * @brief 获取音效的描述符。
     *
     * @param control 指向要调用该接口的音效控件，该指针参数在编译为C接口后产生。
     * @param desc 指定的音效描述符。
     *
     * @return 执行成功返回0，执行失败返回其他值。
     *
     * @since 4.0
     * @version 1.0
     */
    GetEffectDescriptor([out] struct EffectControllerDescriptor desc);

    /**
     * @brief 反转音频处理后的数据。
     *
     * 必须指定输入和输出缓冲区，如果未指定，则反转必须使用命令提供的数据反转功能。
     *
     * @param control 指向要调用该接口的音效控件，该指针参数在编译为C接口后产生。
     * @param input 输入数据buffer。
     * @param output 输出数据buffer。
     *
     * @return 执行成功返回0，执行失败返回其他值。
     *
     * @since 4.0
     * @version 1.0
     */
    EffectReverse([in] struct AudioEffectBuffer input, [out] struct AudioEffectBuffer output);
}
/** @} */
