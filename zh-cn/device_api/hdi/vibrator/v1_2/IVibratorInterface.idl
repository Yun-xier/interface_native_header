/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Vibrator
 * @{
 *
 * @brief 马达驱动对马达服务提供通用的接口能力。
 *
 * 模块提供马达服务对马达驱动访问的统一接口，服务获取驱动对象或者代理后，控制马达的单次振动、周期性振动、高清振动、停止振动、设置马达振幅与频率。
 *
 * @since 4.1
 */

/**
 * @file VibratorTypes.idl
 *
 * @brief 定义马达的通用API，可用于控制马达执行单次、周期性振动或高清振动、设置马达振幅与频率。
 *
 * 模块包路径：ohos.hdi.vibrator.v1_2
 *
 * 引用：
 * - ohos.hdi.vibrator.v1_2.VibratorTypes
 * - ohos.hdi.vibrator.v1_1.IVibratorInterface
 *
 * @since 4.1
 * @version 1.2
 */


package ohos.hdi.vibrator.v1_2;

import ohos.hdi.vibrator.v1_2.VibratorTypes;
import ohos.hdi.vibrator.v1_1.IVibratorInterface;

 /**
 * @brief Vibrator模块向上层服务提供统一的接口。
 *
 * 上层服务开发人员可根据Vibrator模块提供的统一接口，用于控制马达执行单次或周期性振动。
 *
 * @since 4.1
 * @version 1.2
 */
interface IVibratorInterface extends ohos.hdi.vibrator.v1_1.IVibratorInterface{
    /**
     * @brief 高清振动数据下发。
     *
     * @param pkg 表示高清振动数据的数据包，是一个结构体，内部赋值具体振动参数。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.1
     * @version 1.2
     */
    PlayHapticPattern([in] struct HapticPaket pkg);
    /**
     * @brief 获取马达振动能力。
     *
     * @param HapticCapacity 表示振动能力数据包，属性包含是否高清振动，是否支持延时振动，是否支持预定义振动。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.1
     * @version 1.2
     */
    GetHapticCapacity([out] struct HapticCapacity HapticCapacity);
    /**
     * @brief 获取起振时间。
     *
     * @param mode 表示振动模式，按照模式去获取。
     * @param startUpTime 表示从下达振动振动命令到马达振动起来的时间。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.1
     * @version 1.2
     */
    GetHapticStartUpTime([in] int mode, [out] int startUpTime);
    /**
     * @brief 停止马达振动。
     *
     * 马达启动前，必须在任何模式下停止振动。此功能用在振动过程之后。
     *
     * @param mode 表示振动模式，可以是单次或周期性或者HD的，详见{@link HdfVibratorModeV1_2}。
     *
     * @return 如果操作成功，则返回0。
     * @return 如果操作失败，则返回负值。
     *
     * @since 4.1
     * @version 1.2
     */
    StopV1_2([in] int mode);
}
/** @} */