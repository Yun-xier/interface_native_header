/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup power
 * @{
 *
 * @brief 提供休眠/唤醒操作、订阅休眠/唤醒状态、运行锁管理的接口。
 *
 * 电源模块为电源服务提供的休眠/唤醒操作、订阅休眠/唤醒状态和运行锁管理的接口。
 * 服务获取此模块的对象或代理后，可以调用相关的接口对设备进行休眠/唤醒、订阅休眠/唤醒状态和管理运行锁。
 *
 * @since 3.1
 * @version 1.0
 */

/**
 * @file RunningLockTypes.idl
 *
 * @brief 枚举与运行锁管理相关的数据类型。
 *
 * 这些数据类型包括运行锁类型和运行锁信息。
 *
 * 模块包路径：ohos.hdi.power.v1_1
 *
 * @since 4.0
 * @version 1.1
 */

package ohos.hdi.power.v1_1;

/**
 * @brief 枚举基本运行锁类型。
 *
 * @since 4.0
 */
enum BaseRunningLockType {
    /**
     * 用于保持屏幕处于开启状态。
     */
    RUNNINGLOCK_SCREEN = 0,
    /**
     * 用于保持 CPU 处于运行状态，锁屏状态下继续完成后台任务。
     */
    RUNNINGLOCK_BACKGROUND = 1,
    /**
     * 通过传感器控制屏幕的开关。
     */
    RUNNINGLOCK_PROXIMITY_SCREEN_CONTROL = 2,
};

/**
 * @brief 枚举运行锁类型。
 *
 * @since 4.0
 */
enum RunningLockType {
    /**
     * 用于保持后台手机任务的完成。
     */
    RUNNINGLOCK_BACKGROUND_PHONE = 3, // RUNNINGLOCK_BACKGROUND | 1 << 1 = 0b00000011
    /**
     * 用于保持后台通知任务完成。
     */
    RUNNINGLOCK_BACKGROUND_NOTIFICATION = 5, // RUNNINGLOCK_BACKGROUND | 1 << 2 = 0b00000101
    /**
     * 用于保持后台音频任务完成。
     */
    RUNNINGLOCK_BACKGROUND_AUDIO = 9, // RUNNINGLOCK_BACKGROUND | 1 << 3 = 0b00001001
    /**
     * 用于保持后台运动任务的完成。
     */
    RUNNINGLOCK_BACKGROUND_SPORT = 17, // RUNNINGLOCK_BACKGROUND | 1 << 4 = 0b00010001
    /**
     * 用于保持后台导航任务的完成。
     */
    RUNNINGLOCK_BACKGROUND_NAVIGATION = 33, // RUNNINGLOCK_BACKGROUND | 1 << 5 = 0b00100001
    /**
     * 用于保持后台常见任务的完成。
     */
    RUNNINGLOCK_BACKGROUND_TASK = 65, // RUNNINGLOCK_BACKGROUND | 1 << 6 = 0b01000001
    /**
     * 预留运行锁类型。
     */
    RUNNINGLOCK_BUTT
};

/**
 * @brief 定义运行锁的信息。
 *
 * @since 4.0
 */
struct RunningLockInfo {
    /** 运行锁的名称。不能为空  */
    String name;
    /** 运行锁类型，默认值为RUNNINGLOCK_BACKGROUND_TASK */
    enum RunningLockType type; 
    /** 运行锁的超时时间，单位毫秒。值小于 0 表示没有超时。默认为3000 */
    int timeoutMs; 
    /** 进程ID */
    int pid;
    /** 用户ID */
    int uid;
};
/** @} */
