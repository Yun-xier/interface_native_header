/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef RELATIONAL_STORE_ERRNO_CODE_H
#define RELATIONAL_STORE_ERRNO_CODE_H

/**
 * @addtogroup RDB
 * @{
 *
 * The relational database (RDB) store manages data based on relational models.
 * A complete set of mechanisms for managing local databases is provided based on the underlying SQLite.
 * To satisfy different needs in complicated scenarios, the RDB module provides a series of methods for performing
 * operations such as adding, deleting, modifying, and querying data, and supports direct execution of SQL statements.
 *
 * @syscap SystemCapability.DistributedDataManager.RelationalStore.Core
 * @since 10
 */


/**
 * @file relational_store_error_code.h
 *
 * @brief Declares the error code information about a relational database (RDB) store.
 * @library native_rdb_ndk_header.so
 * @since 10
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the error codes.
 *
 * @since 10
 */
typedef enum OH_Rdb_ErrCode {
    /**
     * Execution failed.
     */
    RDB_ERR = -1,

    /**
     * Execution successful.
     */
    RDB_OK = 0,

    /**
     * @brief Error code base.
     */
    E_BASE = 14800000,

    /**
     * @brief Capability not supported.
     */
    RDB_E_NOT_SUPPORTED = 801,

    /**
     * @brief Error codes for common exceptions.
     */
    RDB_E_ERROR = E_BASE,

    /**
     * @brief Invalid parameter.
     */
    RDB_E_INVALID_ARGS = (E_BASE + 1),

    /**
     * Failed to update data because the RDB store is read-only.
     */
    RDB_E_CANNOT_UPDATE_READONLY = (E_BASE + 2),

    /**
     * @brief Failed to delete the file.
     */
    RDB_E_REMOVE_FILE = (E_BASE + 3),

    /**
     * @brief The table name is empty.
     */
    RDB_E_EMPTY_TABLE_NAME = (E_BASE + 5),

    /**
     * @brief The KV pair is empty.
     */
    RDB_E_EMPTY_VALUES_BUCKET = (E_BASE + 6),

    /**
     * @brief Failed to execute the SQL statement for query.
     */
    RDB_E_EXECUTE_IN_STEP_QUERY = (E_BASE + 7),

    /**
     * @brief Invalid column index.
     */
    RDB_E_INVALID_COLUMN_INDEX = (E_BASE + 8),

    /**
     * @brief Invalid column type.
     */
    RDB_E_INVALID_COLUMN_TYPE = (E_BASE + 9),

    /**
     * @brief The file name is empty.
     */
    RDB_E_EMPTY_FILE_NAME = (E_BASE + 10),

    /**
     * @brief Invalid file path.
     */
    RDB_E_INVALID_FILE_PATH = (E_BASE + 11),

    /**
     * @brief Failed to start the transaction.
     */
    RDB_E_TRANSACTION_IN_EXECUTE = (E_BASE + 12),

    /**
     * @brief Failed to precompile the SQL statement.
     */
    RDB_E_INVALID_STATEMENT = (E_BASE + 13),

    /**
     * @brief Failed to write data in a read connection.
     */
    RDB_E_EXECUTE_WRITE_IN_READ_CONNECTION = (E_BASE + 14),

    /**
     * @brief Failed to start the transaction in a read connection.
     */
    RDB_E_BEGIN_TRANSACTION_IN_READ_CONNECTION = (E_BASE + 15),

    /**
     * @brief The transaction to start does not exist in the database session.
     */
    RDB_E_NO_TRANSACTION_IN_SESSION = (E_BASE + 16),

    /**
     * @brief Failed to execute multiple queries a database session.
     */
    RDB_E_MORE_STEP_QUERY_IN_ONE_SESSION = (E_BASE + 17),

    /**
     * @brief The result set does not contain any record.
     */
    RDB_E_NO_ROW_IN_QUERY = (E_BASE + 18),

    /**
     * @brief The number of bound parameters in the SQL statement is invalid.
     */
    RDB_E_INVALID_BIND_ARGS_COUNT = (E_BASE + 19),

    /**
     * @brief Invalid object type.
     */
    RDB_E_INVALID_OBJECT_TYPE = (E_BASE + 20),

    /**
     * @brief Invalid conflict resolution type.
     */
    RDB_E_INVALID_CONFLICT_FLAG = (E_BASE + 21),

    /**
     * @brief The HAVING keyword can be used only after GROUP BY.
     */
    RDB_E_HAVING_CLAUSE_NOT_IN_GROUP_BY = (E_BASE + 22),

    /**
     * @brief The result set in step format is not supported.
     */
    RDB_E_NOT_SUPPORTED_BY_STEP_RESULT_SET = (E_BASE + 23),

    /**
     * @brief Failed to query the result set.
     */
    RDB_E_STEP_RESULT_SET_CROSS_THREADS = (E_BASE + 24),

    /**
     * @brief The result set query statement is not executed.
     */
    RDB_E_STEP_RESULT_QUERY_NOT_EXECUTED = (E_BASE + 25),

    /**
     * @brief The cursor is already in the last row of the result set.
     */
    RDB_E_STEP_RESULT_IS_AFTER_LAST = (E_BASE + 26),

    /**
     * @brief The number of result set query times exceeds the limit.
     */
    RDB_E_STEP_RESULT_QUERY_EXCEEDED = (E_BASE + 27),

    /**
     * @brief The SQL statement is not precompiled.
     */
    RDB_E_STATEMENT_NOT_PREPARED = (E_BASE + 28),

    /**
     * @brief Incorrect database execution result.
     */
    RDB_E_EXECUTE_RESULT_INCORRECT = (E_BASE + 29),

    /**
     * @brief The result set is closed.
     */
    RDB_E_STEP_RESULT_CLOSED = (E_BASE + 30),

    /**
     * @brief Relative path.
     */
    RDB_E_RELATIVE_PATH = (E_BASE + 31),

    /**
     * @brief The new encrypt key file is empty.
     */
    RDB_E_EMPTY_NEW_ENCRYPT_KEY = (E_BASE + 32),

    /**
     * @brief The RDB store is non-encrypted, which is cannot be changed.
     */
    RDB_E_CHANGE_UNENCRYPTED_TO_ENCRYPTED = (E_BASE + 33),

    /**
     * @brief The database does not respond when the database key is updated.
     */
    RDB_E_CHANGE_ENCRYPT_KEY_IN_BUSY = (E_BASE + 34),

    /**
     * @brief The precompiled SQL statement is not initialized.
     */
    RDB_E_STEP_STATEMENT_NOT_INIT = (E_BASE + 35),

    /**
     * @brief The WAL mode does not support the ATTACH operation.
     */
    RDB_E_NOT_SUPPORTED_ATTACH_IN_WAL_MODE = (E_BASE + 36),

    /**
     * @brief Failed to create the folder.
     */
    RDB_E_CREATE_FOLDER_FAIL = (E_BASE + 37),

    /**
     * @brief Failed to build the SQL statements.
     */
    RDB_E_SQLITE_SQL_BUILDER_NORMALIZE_FAIL = (E_BASE + 38),

    /**
     * @brief The database session does not provide a connection.
     */
    RDB_E_STORE_SESSION_NOT_GIVE_CONNECTION_TEMPORARILY = (E_BASE + 39),

    /**
     * @brief The transaction does not exist in the database session.
     */
    RDB_E_STORE_SESSION_NO_CURRENT_TRANSACTION = (E_BASE + 40),

    /**
     * @brief The current operation is not supported.
     */
    RDB_E_NOT_SUPPORT = (E_BASE + 41),

    /**
     * @brief Invalid PARCEL.
     */
    RDB_E_INVALID_PARCEL = (E_BASE + 42),

    /**
     * @brief Failed to execute query.
     */
    RDB_E_QUERY_IN_EXECUTE = (E_BASE + 43),

    /**
     * @brief Failed to set the persistence of the database file in WAL mode.
     */
    RDB_E_SET_PERSIST_WAL = (E_BASE + 44),

    /**
     * @brief The database does not exist.
     */
    RDB_E_DB_NOT_EXIST = (E_BASE + 45),

    /**
     * @brief The number of read connections to set is greater than the limit.
     */
    RDB_E_ARGS_READ_CON_OVERLOAD = (E_BASE + 46),

    /**
     * @brief The WAL log file size exceeds the default value.
    */
    RDB_E_WAL_SIZE_OVER_LIMIT = (E_BASE + 47),

    /**
     * @brief The number of database connections has reached the limit.
     */
    RDB_E_CON_OVER_LIMIT = (E_BASE + 48)
} OH_Rdb_ErrCode;

#ifdef __cplusplus
};
#endif

#endif // RELATIONAL_STORE_ERRNO_CODE_H
